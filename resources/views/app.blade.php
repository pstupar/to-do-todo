<html>
  <head>
    <title>@yield('title')</title>
    <style>
      body {
          padding: 20px;
      }
      
      table {
          border-collapse: collapse;
      }
      
      tr {
          border-bottom: 1px solid #000;
      }

      td {
          vertical-align: middle;
      }

      form {
          margin: 0;
      }

      ul {
          list-style-type: none;
      }
      
      .done {
          text-decoration: line-through;
      }

      .btn {
          background: #fff;
          border: 2px solid #000;
          border-radius: 5px;
          outline:  none;
          color: #000;
          text-decoration: none;
          display: inline-block;
          margin: 5px;
          padding: 5px 15px;
      }

      .btn:hover,
      .btn:active {
          background: #000;
          color: #fff;
          cursor: pointer;
      }
    </style>
  </head>
  <body>
    <div class="container">
      @yield('content')
    </div>
  </body>
</html>