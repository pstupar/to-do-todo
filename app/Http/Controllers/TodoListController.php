<?php

namespace App\Http\Controllers;

use App\TodoItem;
use App\Http\Controllers\Controller;

class TodoListController extends Controller
{
    /**
     * Fetch all entries and display them in the list view
     * 
     * @return view
     */
    public function index(){
        $exampleValue = "Oh, Toto! I've a feeling we're not in Kansas anymore!";
        
        return view('example', ['someVar' => $exampleValue]);
    }
}
